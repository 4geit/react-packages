import React from 'react'
import angular from 'angular'
import { react2angular } from 'react2angular'
import { Provider } from 'mobx-react'

import RctTodoComponent from './rct-todo.component'

const stores = {
  // TBD
}

const RctTodoComponentWrapper = () => (
  <Provider {...stores} >
    <RctTodoComponent />
  </Provider>
)

export default angular
  .module('rct-todo-component', [])
  .component('rctTodoComponent', react2angular(RctTodoComponentWrapper))
  .name
