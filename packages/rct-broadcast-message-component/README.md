# @4geit/rct-broadcast-message-component [![npm version](//badge.fury.io/js/@4geit%2Frct-broadcast-message-component.svg)](//badge.fury.io/js/@4geit%2Frct-broadcast-message-component)

---

Broadcast message component to display an input zone for typing and sending messages

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-broadcast-message-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-broadcast-message-component) package manager using the following command:

```bash
npm i @4geit/rct-broadcast-message-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-broadcast-message-component
```

2. Depending on where you want to use the component you will need to import the class `RctBroadcastMessageComponent` to your project JS file as follows:

```js
import RctBroadcastMessageComponent from '@4geit/rct-broadcast-message-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctBroadcastMessageComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctBroadcastMessageComponent from '@4geit/rct-broadcast-message-component'
// ...
const App = () => (
  <div className="App">
    <RctBroadcastMessageComponent/>
  </div>
)
```
