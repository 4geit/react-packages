import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import eventListStore from '@4geit/rct-event-list-store'
import datePickerStore from '@4geit/rct-date-picker-store'

import RctEventListComponent from './rct-event-list.component'

const debug = buildDebug('react-packages:packages:test:rct-event-list-component')

const stores = {
  commonStore,
  swaggerClientStore,
  eventListStore,
  datePickerStore,
}

process.env.STORYBOOK_API_APP_TOKEN = '1234'
process.env.STORYBOOK_BOOKING_URL = 'https://'

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctEventListComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

const testProps = {
  merchandId: '1234',
  bookingUrl: 'https://',
  topTitle: 'Event list',
}

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores} >
      <RctEventListComponent {...testProps} />
    </Provider>
    , div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount((
//     <Provider {...stores} >
//       <RctEventListComponent {...testProps} />
//     </Provider>
//   ))
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctEventListComponent {...stores} {...testProps} />)
  expect(wrapper).toMatchSnapshot()
})
