import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'
import { observable, action } from 'mobx'
import IconButton from 'material-ui/IconButton'
import ViewColumnIcon from 'material-ui-icons/ViewColumn'
import Menu, { MenuItem } from 'material-ui/Menu'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import Switch from 'material-ui/Switch'
import Tooltip from 'material-ui/Tooltip'

import Column from './column'

const debug = buildDebug('react-packages:packages:rct-data-table-component:column-menu-component')

@observer
export default class ColumnMenuComponent extends Component {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    title: PropTypes.string,
  }
  static defaultProps = {
    title: 'Show/Hide columns',
  }

  @observable open
  @observable element

  @action handleOpen = (event) => {
    debug('handleMenuClick()')
    this.open = true
    this.element = event.currentTarget
  }
  @action handleClose = () => {
    debug('handleClose()')
    this.open = false
  }
  handleChange = item => (event, checked) => {
    debug('handleChange()')
    item.setEnabled(checked)
  }

  render() {
    debug('render()')
    const { columns, title } = this.props
    return (
      <div>
        {/* column button */}
        <Tooltip title={title}>
          <IconButton onClick={this.handleOpen}>
            <ViewColumnIcon />
          </IconButton>
        </Tooltip>
        {/* columns menu */}
        <Menu
          anchorEl={this.element}
          open={this.open}
          onRequestClose={this.handleClose}
          className="menu"
        >
          { columns.map(item => (
            <MenuItem key={item.name} >
              <Grid container alignItems="center">
                <Grid item xs>
                  <Typography>{ item.name }</Typography>
                </Grid>
                <Grid item>
                  <Switch
                    checked={item.enabled}
                    onChange={this.handleChange(item)}
                  />
                </Grid>
              </Grid>
            </MenuItem>
          )) }
        </Menu>
      </div>
    )
  }
}
