import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer, inject } from 'mobx-react'
import { observable, action } from 'mobx'
import csvtojson from 'csvtojson'
import { withStyles } from 'material-ui/styles'
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import ImportExportIcon from 'material-ui-icons/ImportExport'
import Grid from 'material-ui/Grid'
import List, { ListItem, ListItemText } from 'material-ui/List'
import Tooltip from 'material-ui/Tooltip'

import { RctNotificationStore } from '@4geit/rct-notification-store'

import Column from './column'

const debug = buildDebug('react-packages:packages:rct-data-table-component:import-dialog-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  action: {
    width: '100%',
  },
  // TBD
}))
@inject('notificationStore')
@observer
export default class ImportDialogComponent extends Component {
  static propTypes = {
    notificationStore: PropTypes.instanceOf(RctNotificationStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string,
  }
  static defaultProps = {
    title: 'Import new items',
  }

  componentWillMount() {
    debug('componentWillMount()')
    this.reset()
  }

  @action reset = () => {
    debug('reset()')
    this.importedData = []
    this.open = false
  }
  @action handleOpen = () => {
    debug('handleOpen()')
    this.open = true
  }
  @action handleClose = () => {
    debug('handleClose()')
    this.open = false
  }
  @action handleMoreInfo = () => {
    debug('handleMoreInfo()')
    this.columnsShown = !this.columnsShown
  }
  handleReadImportedFile = ({ target: { files: [file] } }) => {
    debug('handleReadImportedFile')
    const { notificationStore, columns } = this.props
    const reader = new FileReader()
    reader.readAsText(file)
    reader.onload = ({ target: { result } }) => {
      debug('onload')
      csvtojson()
        .fromString(result)
        .on('json', (obj) => {
          debug('json')
          const keys = Object.keys(obj)
          if (columns.some(({ name }) => !keys.includes(name))) {
            notificationStore.newMessage('bad format')
            throw new Error('bad format')
          }
        })
        .on('end_parsed', action((data) => {
          debug('end_parsed')
          this.importedData = data
        }))
    }
  }
  @action handleOnSubmit = async (event) => {
    debug('handleOnSubmit()')
    event.preventDefault()
    const { onSubmit } = this.props
    await onSubmit(this.importedData)
    this.reset()
  }

  @observable fields = {}
  @observable open = false
  @observable columnsShown = false
  @observable importedData = []

  render() {
    const { classes, columns, title } = this.props
    return (
      <div>
        {/* import button */}
        <Tooltip title={title}>
          <IconButton onClick={this.handleOpen}>
            <ImportExportIcon />
          </IconButton>
        </Tooltip>
        {/* import form dialog */}
        <Dialog
          open={this.open}
          onRequestClose={this.handleClose}
        >
          <DialogTitle>Import items</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <span>
                You can import a CSV file. The CSV file should contains the following fields:
              </span>
            </DialogContentText>
            { this.columnsShown && (
              <List>
                { columns.map(({ name }) => (
                  <ListItem dense key={name}>
                    <ListItemText primary={name} />
                  </ListItem>
                )) }
              </List>
            ) }
            <input
              id="file"
              type="file"
              onChange={this.handleReadImportedFile}
              style={{
                width: 0,
                height: 0,
                opacity: 0,
                overflow: 'hidden',
                position: 'absolute',
                zIndex: 1,
              }}
            />
            <Button component="label" htmlFor="file">
              Choose a file
            </Button>
          </DialogContent>
          <DialogActions
            classes={{
              action: classes.action,
            }}
          >
            <Grid container>
              <Grid item xs>
                <Button color="accent" onClick={this.handleMoreInfo}>More Info</Button>
              </Grid>
              <Grid item>
                <Button onClick={this.handleClose}>Cancel</Button>
                <Button onClick={this.handleOnSubmit} raised color="primary">Import</Button>
              </Grid>
            </Grid>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
