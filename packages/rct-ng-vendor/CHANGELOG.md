# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.101.2"></a>
## [1.101.2](https://gitlab.com/4geit/react-packages/compare/v1.101.1...v1.101.2) (2018-01-13)


### Bug Fixes

* **project-builder:** fix minor issue + improved vendor component ([5f28826](https://gitlab.com/4geit/react-packages/commit/5f28826))




<a name="1.101.0"></a>
# [1.101.0](https://gitlab.com/4geit/react-packages/compare/v1.100.1...v1.101.0) (2017-12-19)




**Note:** Version bump only for package @4geit/rct-ng-vendor
