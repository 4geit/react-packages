# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.2"></a>
## [1.103.2](https://gitlab.com/4geit/react-packages/compare/v1.103.1...v1.103.2) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-conway-component

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-conway-component

<a name="1.97.1"></a>
## [1.97.1](https://gitlab.com/4geit/react-packages/compare/v1.97.0...v1.97.1) (2017-12-07)


### Bug Fixes

* **conway:** minor ([d561b63](https://gitlab.com/4geit/react-packages/commit/d561b63))




<a name="1.97.0"></a>
# [1.97.0](https://gitlab.com/4geit/react-packages/compare/v1.96.0...v1.97.0) (2017-12-07)


### Bug Fixes

* **conway-component:** minor ([f902481](https://gitlab.com/4geit/react-packages/commit/f902481))


### Features

* **conway component:** completions ([44f4d97](https://gitlab.com/4geit/react-packages/commit/44f4d97))




<a name="1.96.0"></a>
# [1.96.0](https://gitlab.com/4geit/react-packages/compare/v1.95.0...v1.96.0) (2017-12-06)


### Features

* **componens:** add new componets as part of the conways game implementation game ([2f19544](https://gitlab.com/4geit/react-packages/commit/2f19544))
