import React from 'react'
import angular from 'angular'
import { react2angular } from 'react2angular'
import { Provider } from 'mobx-react'

import RctCollapsibleComponent from './rct-collapsible.component'

const stores = {
  // TBD
}

const RctCollapsibleComponentWrapper = () => (
  <Provider {...stores} >
    <RctCollapsibleComponent />
  </Provider>
)

export default angular
  .module('rct-collapsible-component', [])
  .component('rctCollapsibleComponent', react2angular(RctCollapsibleComponentWrapper))
  .name
