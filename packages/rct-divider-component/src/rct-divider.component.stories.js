import React from 'react'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
import { withInfo } from '@storybook/addon-info'

import RctDividerComponent from './rct-divider.component'

storiesOf('RctDividerComponent', module)
  .addDecorator(centered)
  .add('simple usage', withInfo()(() => (
    <RctDividerComponent />
  )))
