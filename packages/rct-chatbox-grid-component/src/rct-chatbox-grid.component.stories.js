/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { storiesOf } from '@storybook/react'
// eslint-disable-next-line no-unused-vars
import centered from '@storybook/addon-centered'
// eslint-disable-next-line no-unused-vars
import { withInfo } from '@storybook/addon-info'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import chatboxGridStore from '@4geit/rct-chatbox-grid-store'

import RctBroadcastMessageComponent from '@4geit/rct-broadcast-message-component'
import RctChatboxGridComponent from './rct-chatbox-grid.component'

const stores = {
  commonStore,
  swaggerClientStore,
  chatboxGridStore,
}

class ErrorBoundary extends React.Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  // eslint-disable-next-line no-unused-vars
  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true })
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, info)
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <h1>Something went wrong.</h1>
      )
    }
    return this.props.children
  }
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctChatboxGridComponent', module)
  .add('simple usage', () => (
    <Provider {...stores} >
      <App>
        <ErrorBoundary>
          <RctChatboxGridComponent />
        </ErrorBoundary>
      </App>
    </Provider>
  ))
  .add('chatboxgrid with broadcast message component', () => (
    <Provider {...stores} >
      <App>
        <ErrorBoundary>
          <RctChatboxGridComponent />
          <RctBroadcastMessageComponent helper="Send a message to all active chatboxes!" label="Your message" />
        </ErrorBoundary>
      </App>
    </Provider>
  ))
