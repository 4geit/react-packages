# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.2"></a>
## [1.103.2](https://gitlab.com/4geit/react-packages/compare/v1.103.1...v1.103.2) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-landing-page-layout-component

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-landing-page-layout-component

<a name="1.97.4"></a>
## [1.97.4](https://gitlab.com/4geit/react-packages/compare/v1.97.3...v1.97.4) (2017-12-08)


### Bug Fixes

* **landing-page-layout:** fix layout ([03e3d1c](https://gitlab.com/4geit/react-packages/commit/03e3d1c))




<a name="1.95.0"></a>
# [1.95.0](https://gitlab.com/4geit/react-packages/compare/v1.94.0...v1.95.0) (2017-12-05)


### Features

* **landing-page-layout-component:** completions ([1dd57da](https://gitlab.com/4geit/react-packages/commit/1dd57da))




<a name="1.94.0"></a>
# [1.94.0](https://gitlab.com/4geit/react-packages/compare/v1.93.0...v1.94.0) (2017-12-05)


### Features

* **landing-page-layout:** add new component ([d139640](https://gitlab.com/4geit/react-packages/commit/d139640))
