/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Section extends Component {
  static propTypes = {
    /* eslint-disable react/no-unused-prop-types */
    backgroundImage: PropTypes.string,
    backgroundColor: PropTypes.string,
    /* eslint-enable react/no-unused-prop-types */
    // eslint-disable-next-line react/forbid-prop-types
    component: PropTypes.any.isRequired,
  }
  static defaultProps = {
    backgroundImage: undefined,
    backgroundColor: undefined,
  }

  render() {
    const { component } = this.props
    return (
      component
    )
  }
}
