import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
// eslint-disable-next-line no-unused-vars
import { withInfo } from '@storybook/addon-info'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import chatboxListStore from '@4geit/rct-chatbox-list-store'
import chatboxGridStore from '@4geit/rct-chatbox-grid-store'

import RctChatboxListComponent from './rct-chatbox-list.component'

const stores = {
  commonStore,
  swaggerClientStore,
  chatboxListStore,
  chatboxGridStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctChatboxListComponent', module)
  .addDecorator(centered)
  .add('simple usage', () => (
    <Provider {...stores}>
      <App>
        <RctChatboxListComponent />
      </App>
    </Provider>
  ))
