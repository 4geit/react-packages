import { observable, computed, action, runInAction } from 'mobx'
// eslint-disable-next-line no-unused-vars
import { hashHistory } from 'react-router'
import buildDebug from 'debug'
import moment from 'moment'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

const debug = buildDebug('react-packages:packages:rct-chatbox-grid-store')

export class RctChatboxGridStore {
  @observable data = []
  @observable maximizedItem
  @computed get sortedData() {
    debug('sortedData()')
    return this.data.slice().sort((a, b) => a.position > b.position)
  }
  @observable inProgress = false

  @action setData(value) {
    debug('setData()')
    this.data = value
  }
  @action setMaximizedItem(value) {
    debug('setMaximizedItem()')
    this.maximizedItem = value
  }
  @action removeMaximizedItem() {
    debug('removeMaximizedItem()')
    this.maximizedItem = undefined
  }
  @action async fetchData({ listOperationId }) {
    debug('fetchData()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId || 'userChatboxList']({})
      runInAction(() => {
        this.setData(body)
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async setPosition({ updateOperationId, itemId, position }) {
    debug('setPosition()')
    debug(itemId)
    debug(position)
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[updateOperationId || 'userChatboxUpdate']({
        id: itemId,
        body: {
          position,
        },
      })
      runInAction(() => {
        if (body.length) {
          this.setData(body)
        }
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async addItem({ addOperationId, listOperationId }) {
    debug('addItem()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[addOperationId || 'userChatboxAdd']()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async deleteItem({ deleteOperationId, itemId }) {
    debug('deleteItem()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[deleteOperationId || 'userChatboxDelete']({ id: itemId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async activateItem({ updateOperationId, listOperationId }) {
    debug('activateItem()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[updateOperationId || 'userChatboxUpdate']()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async unactivateItem({ updateOperationId, listOperationId }) {
    debug('unactivateItem()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[updateOperationId || 'userChatboxUpdate']()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async fetchMaximizedItem({ listOperationId }) {
    debug('fetchMaximizedItem()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId || 'userChatboxList']({
        maximized: true,
      })
      runInAction(() => {
        if (body.length) {
          const [item] = body
          this.setMaximizedItem(item)
        } else {
          this.removeMaximizedItem()
        }
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async toggleMaximize({
    bulkUpdateOperationId, updateOperationId, itemId, maximized,
  }) {
    debug('toggleMaximize()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      // first reset all user chatboxes maximized value to false
      await Account[bulkUpdateOperationId || 'userChatboxBulkUpdate']({
        body: { maximized: false },
      })
      // and then set only the one selected
      await Account[updateOperationId || 'userChatboxUpdate']({
        id: itemId,
        body: { maximized },
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
  @action async sendMessage({ bulkAddOperationId, message, author }) {
    debug('sendMessage()')
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[bulkAddOperationId || 'messageBulkAdd']({
        body: {
          message,
          author,
          date: moment(),
        },
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
        this.inProgress = false
      })
    }
  }
}

export default new RctChatboxGridStore()
