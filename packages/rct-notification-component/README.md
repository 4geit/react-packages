# @4geit/rct-notification-component [![npm version](//badge.fury.io/js/@4geit%2Frct-notification-component.svg)](//badge.fury.io/js/@4geit%2Frct-notification-component)

---

notification component to display a snackbar on the bottom

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-notification-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-notification-component) package manager using the following command:

```bash
npm i @4geit/rct-notification-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-notification-component
```

2. Depending on where you want to use the component you will need to import the class `RctNotificationComponent` to your project JS file as follows:

```js
import RctNotificationComponent from '@4geit/rct-notification-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctNotificationComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctNotificationComponent from '@4geit/rct-notification-component'
// ...
const App = () => (
  <div className="App">
    <RctNotificationComponent/>
  </div>
)
```
