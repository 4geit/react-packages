# @4geit/rct-project-component [![npm version](//badge.fury.io/js/@4geit%2Frct-project-component.svg)](//badge.fury.io/js/@4geit%2Frct-project-component)

---

main component used to bootstrap the other components

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-project-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-project-component) package manager using the following command:

```bash
npm i @4geit/rct-project-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-project-component
```

2. Depending on where you want to use the component you will need to import the class `RctProjectComponent` to your project JS file as follows:

```js
import RctProjectComponent from '@4geit/rct-project-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctProjectComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctProjectComponent from '@4geit/rct-project-component'
// ...
const App = () => (
  <div className="App">
    <RctProjectComponent/>
  </div>
)
```
