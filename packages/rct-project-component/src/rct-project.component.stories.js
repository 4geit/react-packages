import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import Typography from 'material-ui/Typography'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import authStore from '@4geit/rct-auth-store'
import notificationStore from '@4geit/rct-notification-store'

import RctLayoutComponent from '@4geit/rct-layout-component'
import RctHeaderComponent from '@4geit/rct-header-component'
import RctSideMenuComponent, { SideMenuItem } from '@4geit/rct-side-menu-component'
import RctLoginComponent from '@4geit/rct-login-component'
import RctRegisterComponent from '@4geit/rct-register-component'
import RctProjectComponent from './rct-project.component'
import Logo from './assets/logo.png'

const stores = {
  commonStore,
  swaggerClientStore,
  authStore,
  notificationStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

const HomeComponent = () => (
  <Typography type="title">Home</Typography>
)

storiesOf('RctProjectComponent', module)
  .add('with layout, side-menu and switch', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores}>
        <App>
          <RctProjectComponent
            layoutComponent={
              <RctLayoutComponent
                topComponent={<RctHeaderComponent logo={Logo} />}
                sideMenuComponent={
                  <RctSideMenuComponent>
                    <SideMenuItem icon="home" label="Home" route="/" divider />
                    <SideMenuItem icon="account_box" label="Login" route="/login" />
                    <SideMenuItem icon="fiber_new" label="Register" route="/register" />
                  </RctSideMenuComponent>
                }
              />
            }
          >
            <Switch>
              <Route path="/login" component={RctLoginComponent} />
              <Route path="/register" component={RctRegisterComponent} />
              <Route path="/" component={HomeComponent} />
            </Switch>
          </RctProjectComponent>
        </App>
      </Provider>
    </BrowserRouter>
  )))
