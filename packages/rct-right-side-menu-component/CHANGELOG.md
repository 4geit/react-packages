# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.2"></a>
## [1.103.2](https://gitlab.com/4geit/react-packages/compare/v1.103.1...v1.103.2) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)


### Bug Fixes

* **chatbox-grid:** fix maximizing + add tooltips ([ab62254](https://gitlab.com/4geit/react-packages/commit/ab62254))




<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)


### Bug Fixes

* **components:** center data-table progress icon, add new class to override width right-side-menu ([bd644d8](https://gitlab.com/4geit/react-packages/commit/bd644d8))
* **side-menu:** remove overflow hidden css property ([96fcf16](https://gitlab.com/4geit/react-packages/commit/96fcf16))




<a name="1.87.1"></a>
## [1.87.1](https://gitlab.com/4geit/react-packages/compare/v1.87.0...v1.87.1) (2017-11-23)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.80.5"></a>
## [1.80.5](https://gitlab.com/4geit/react-packages/compare/v1.80.4...v1.80.5) (2017-10-30)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.74.1"></a>
## [1.74.1](https://gitlab.com/4geit/react-packages/compare/v1.74.0...v1.74.1) (2017-10-13)


### Bug Fixes

* **right-side-menu-component:** fix error in sidemenu and content ([579064e](https://gitlab.com/4geit/react-packages/commit/579064e))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.40.0"></a>
# [1.40.0](https://gitlab.com/4geit/react-packages/compare/v1.39.4...v1.40.0) (2017-09-22)


### Features

* **event-list-component:** add pagination feature ([dd38138](https://gitlab.com/4geit/react-packages/commit/dd38138))




<a name="1.37.0"></a>
# [1.37.0](https://gitlab.com/4geit/react-packages/compare/v1.36.0...v1.37.0) (2017-09-20)


### Features

* **event-list-component:** improved UI component + integrate new API structure ([e7e0805](https://gitlab.com/4geit/react-packages/commit/e7e0805))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-right-side-menu-component

<a name="1.30.0"></a>
# [1.30.0](https://gitlab.com/4geit/react-packages/compare/v1.29.3...v1.30.0) (2017-09-17)


### Features

* **RctRightSideMenu:** add chatboxList to rightSideMenu ([a54ddc9](https://gitlab.com/4geit/react-packages/commit/a54ddc9))




<a name="1.29.0"></a>
# [1.29.0](https://gitlab.com/4geit/react-packages/compare/v1.28.0...v1.29.0) (2017-09-14)


### Bug Fixes

* **layout-component:** fix issue with rightSideMenuComponent no showing and duplication ([9fbecdd](https://gitlab.com/4geit/react-packages/commit/9fbecdd))


### Features

* **right-side-menu:** implementing right side menu to the main layout component ([afe3f5e](https://gitlab.com/4geit/react-packages/commit/afe3f5e))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/react-packages/compare/v1.26.0...v1.28.0) (2017-09-12)


### Bug Fixes

* **minor:** minor ([e38ae40](https://gitlab.com/4geit/react-packages/commit/e38ae40))


### Features

* **right-side-menu:** add right-side-menu component with permament type ([004b5ce](https://gitlab.com/4geit/react-packages/commit/004b5ce))




<a name="1.27.0"></a>
# [1.27.0](https://gitlab.com/4geit/react-packages/compare/v1.26.0...v1.27.0) (2017-09-12)


### Bug Fixes

* **minor:** minor ([e38ae40](https://gitlab.com/4geit/react-packages/commit/e38ae40))


### Features

* **right-side-menu:** add right-side-menu component with permament type ([004b5ce](https://gitlab.com/4geit/react-packages/commit/004b5ce))
