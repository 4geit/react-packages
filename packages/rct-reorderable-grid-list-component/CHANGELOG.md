# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.2"></a>
## [1.103.2](https://gitlab.com/4geit/react-packages/compare/v1.103.1...v1.103.2) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)


### Bug Fixes

* **chatbox-grid:** fix test issue ([c68b3cd](https://gitlab.com/4geit/react-packages/commit/c68b3cd))
* **chatbox-grid:** improve maximize page ([b0b1528](https://gitlab.com/4geit/react-packages/commit/b0b1528))




<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)


### Bug Fixes

* **chatbox-list:** fix switch issue, pass correct userchatbox id to remove item, update the userchat ([5823c9d](https://gitlab.com/4geit/react-packages/commit/5823c9d))




<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.85.1"></a>
## [1.85.1](https://gitlab.com/4geit/react-packages/compare/v1.85.0...v1.85.1) (2017-11-18)


### Bug Fixes

* **reorderable-grid-list:** fix test issue ([94da3e3](https://gitlab.com/4geit/react-packages/commit/94da3e3))




<a name="1.84.3"></a>
## [1.84.3](https://gitlab.com/4geit/react-packages/compare/v1.84.2...v1.84.3) (2017-11-16)


### Bug Fixes

* **chatbox-grid:** fix reorderable grid issue ([9b53b12](https://gitlab.com/4geit/react-packages/commit/9b53b12))




<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.82.5"></a>
## [1.82.5](https://gitlab.com/4geit/react-packages/compare/v1.82.4...v1.82.5) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-reorderable-grid-list-component

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.61.0"></a>
# [1.61.0](https://gitlab.com/4geit/react-packages/compare/v1.60.0...v1.61.0) (2017-10-06)


### Features

* **rct-reorderable-grid-list:** pass `item` as a prop to grid-tile ([d170145](https://gitlab.com/4geit/react-packages/commit/d170145))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.45.0"></a>
# [1.45.0](https://gitlab.com/4geit/react-packages/compare/v1.44.5...v1.45.0) (2017-10-02)


### Bug Fixes

* **ChatboxGridComponent:** css minor change ([536b7de](https://gitlab.com/4geit/react-packages/commit/536b7de))


### Features

* **ChatboxGrid:** Added Avatar, minor css change ([031b032](https://gitlab.com/4geit/react-packages/commit/031b032))
* **style chatbox:** added css ([5acf043](https://gitlab.com/4geit/react-packages/commit/5acf043))
* **Style itemcomponent:** css ([f6bdbc6](https://gitlab.com/4geit/react-packages/commit/f6bdbc6))




<a name="1.44.3"></a>
## [1.44.3](https://gitlab.com/4geit/react-packages/compare/v1.44.2...v1.44.3) (2017-09-27)


### Bug Fixes

* **reorderable-grid-list:** fix setPosition call with destructured param instead of fixed params ([3e64768](https://gitlab.com/4geit/react-packages/commit/3e64768))




<a name="1.42.0"></a>
# [1.42.0](https://gitlab.com/4geit/react-packages/compare/v1.41.0...v1.42.0) (2017-09-22)


### Features

* **rct-chatbox-grid-component:** start integrating reorderable-grid-list-component ([9e4febd](https://gitlab.com/4geit/react-packages/commit/9e4febd))
* **rct-reorderable-grid-list-component:** add itemComponent prop to set component to each tile of t ([5157ae0](https://gitlab.com/4geit/react-packages/commit/5157ae0))
* **rct-reorderable-grid-list-component:** tile component now works with source and target, remove s ([75a6257](https://gitlab.com/4geit/react-packages/commit/75a6257))
* **reorderable-grid-list-component:** add a new reorderable grid list component + refactor componen ([09fa0da](https://gitlab.com/4geit/react-packages/commit/09fa0da))
