import React from 'react'
import angular from 'angular'
import { react2angular } from 'react2angular'
import { Provider } from 'mobx-react'

import RctAssemblerComponent from './rct-assembler.component'

const stores = {
  // TBD
}

const RctAssemblerComponentWrapper = () => (
  <Provider {...stores} >
    <RctAssemblerComponent />
  </Provider>
)

export default angular
  .module('rct-assembler-component', [])
  .component('rctAssemblerComponent', react2angular(RctAssemblerComponentWrapper))
  .name
