import { observable, action, runInAction, toJS } from 'mobx'
// eslint-disable-next-line no-unused-vars
import { hashHistory } from 'react-router'
import buildDebug from 'debug'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'

const debug = buildDebug('react-packages:packages:rct-auth-store')

export class RctAuthStore {
  @observable inProgress = false
  @observable values = {
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    remember: false,
    acknowledge: false,
  }

  @action setEmail(value) {
    debug('setEmail()')
    this.values.email = value
  }
  @action setPassword(value) {
    debug('setPassword()')
    this.values.password = value
  }
  @action setRemember(value) {
    debug('setRemember()')
    this.values.remember = value
  }
  @action setAcknowledge(value) {
    debug('setAcknowledge()')
    this.values.acknowledge = value
  }
  @action reset() {
    debug('reset()')
    this.values.email = ''
    this.values.password = ''
    this.values.firstname = ''
    this.values.lastname = ''
    this.values.remember = false
    this.values.acknowledge = false
  }
  @action async login() {
    debug('login()')
    this.inProgress = true
    debug('Sending form', JSON.stringify(toJS(this.values), null, 2))
    try {
      const { email, password } = this.values
      const { body } = await swaggerClientStore.client.apis.Account.login({
        account: { email, password },
      })
      const {
        token, firstname, lastname, email: emailField,
        password: passwordField, company, address, phone,
      } = body
      debug(body)
      commonStore.setToken(token)
      commonStore.setUser({
        firstname, lastname, email: emailField, password: passwordField, company, address, phone,
      })
      await swaggerClientStore.buildClientWithToken({ token })
      runInAction(() => {
        this.inProgress = false
        notificationStore.newMessage('You are logged in!')
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async register() {
    debug('register()')
    this.inProgress = true
    debug('Sending form', JSON.stringify(toJS(this.values), null, 2))
    try {
      const {
        email, password, firstname, lastname, acknowledge,
      } = this.values
      if (!acknowledge) {
        notificationStore.newMessage('You have to agree on the registration terms.')
        this.inProgress = false
        return
      }
      const { body } = await swaggerClientStore.client.apis.Account.register({
        account: {
          email, password, firstname, lastname,
        },
      })
      debug(body)
      runInAction(() => {
        this.inProgress = false
        notificationStore.newMessage('You are registered!')
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctAuthStore()
