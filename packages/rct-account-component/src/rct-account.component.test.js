import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'

import commonStore from '@4geit/rct-common-store'
import accountStore from '@4geit/rct-account-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

import RctAccountComponent from './rct-account.component'

const debug = buildDebug('react-packages:packages:test:rct-account-component')

const stores = {
  commonStore,
  accountStore,
  swaggerClientStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctAccountComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores}>
      <RctAccountComponent />
    </Provider>,
    div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount(<RctAccountComponent {...stores} />)
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow(<RctAccountComponent {...stores} />)
  expect(wrapper).toMatchSnapshot()
})

describe('unlogged', () => {
  const tmpUser = commonStore.user
  afterAll(() => {
    commonStore.user = tmpUser
  })
  test('given the user is unlogged', () => {
    debug('given the user is unlogged')
    commonStore.user = undefined
    expect(commonStore.user).toBeUndefined()
  })
  test('then the user should see a loading message', () => {
    debug('then the user should see a loading message')
    const wrapper = shallow(<RctAccountComponent {...stores} />)
    expect(wrapper.text()).toContain('Loading...')
  })
})
describe('logged-in', () => {
  debug('logged-in')
  describe('instructions', () => {
    debug('instructions')
    test('then the user should see some instructions', () => {
      debug('then the user should see some instructions')
      const wrapper = mount(<RctAccountComponent {...stores} />)
      expect(wrapper.text()).toContain('Fill out the fields above with your information.')
    })
  })
  describe('fields', () => {
    debug('fields')
    test('then the user should see the fields', () => {
      debug('then the user should see the fields')
      const wrapper = shallow(<RctAccountComponent {...stores} />)
      'Email,Password,Firstname,Lastname,Company,Street,State,Postcode,Country,Phone'.split(',').forEach((label) => {
        expect(wrapper
          .find(TextField)
          .find({ label, fullWidth: true })).toHaveLength(1)
      })
    })
  })
  describe('update button', () => {
    debug('update button')
    test('then the user should see the update button', () => {
      debug('then the user should see the update button')
      const wrapper = shallow(<RctAccountComponent {...stores} />)
      expect(wrapper
        .find(Button)
        .find({ color: 'primary' })).toHaveLength(1)
      // TODO: dont know how to test a shallowed Button content so far
      // expect(wrapper.find(Button).text()).toContain('Update')
    })
  })
  describe('fields values available', () => {
    debug('fields values available')
    test('then the user should see the user infos', () => {
      debug('then the user should see the user infos')
      const wrapper = shallow(<RctAccountComponent {...stores} />)
      const fields = [
        { label: 'Email', value: 'aaaa@bbbb.xyz' },
        { label: 'Password', value: 'passw0rd' },
        { label: 'Firstname', value: 'aaaa' },
        { label: 'Firstname', value: 'aaaa' },
        { label: 'Lastname', value: 'bbbb' },
        { label: 'Company', value: 'my company' },
        { label: 'Street', value: 'first street' },
        { label: 'State', value: 'poney' },
        { label: 'Postcode', value: '1234' },
        { label: 'Country', value: 'unicorn' },
        { label: 'Phone', value: '1111-222-333' },
      ]
      fields.forEach(({ label, value }) => {
        expect(wrapper
          .find(TextField)
          .find({ label, value })).toHaveLength(1)
      })
    })
  })
  // TODO: test not working for now
  // describe('change fields values', () => {
  //   debug('change fields values')
  //   let wrapper
  //   beforeAll(() => {
  //     wrapper = shallow(
  //       <RctAccountComponent { ...stores } />
  //     )
  //   })
  //   test('when the user changes the value of email field', () => {
  //     debug('when the user changes the value of email field')
  //     wrapper
  //       .find(TextField)
  //       .find({ label: 'Email' })
  //       .simulate('change', {
  //         target: {
  //           value: 'xxxx@yyyy.zzz',
  //         },
  //       })
  //   })
  //   test('then the email field value should be changed', () => {
  //     debug('then the email field value should be changed')
  //     expect(
  //       wrapper
  //         .find(TextField)
  //         .find({ label: 'Email', value: 'xxxx@yyyy.zzz' })
  //     ).toHaveLength(1)
  //   })
  // })
})
