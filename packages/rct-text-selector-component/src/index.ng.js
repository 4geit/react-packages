import React from 'react'
import angular from 'angular'
import { react2angular } from 'react2angular'
import { Provider } from 'mobx-react'

import RctTextSelectorComponent from './rct-text-selector.component'

const stores = {
  // TBD
}

const RctTextSelectorComponentWrapper = () => (
  <Provider {...stores} >
    <RctTextSelectorComponent />
  </Provider>
)

export default angular
  .module('rct-text-selector-component', [])
  .component('rctTextSelectorComponent', react2angular(RctTextSelectorComponentWrapper))
  .name
