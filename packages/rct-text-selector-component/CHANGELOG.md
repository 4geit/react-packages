# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.2"></a>
## [1.103.2](https://gitlab.com/4geit/react-packages/compare/v1.103.1...v1.103.2) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-text-selector-component

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-text-selector-component

<a name="1.103.0"></a>
# [1.103.0](https://gitlab.com/4geit/react-packages/compare/v1.102.4...v1.103.0) (2018-02-08)


### Bug Fixes

* **minor:** remove deprecated package ([4d799f3](https://gitlab.com/4geit/react-packages/commit/4d799f3))


### Features

* **Prepare challenge:** prepared files and dependencies ([687a145](https://gitlab.com/4geit/react-packages/commit/687a145))




<a name="1.102.4"></a>
## [1.102.4](https://gitlab.com/4geit/react-packages/compare/v1.102.3...v1.102.4) (2018-02-06)




**Note:** Version bump only for package @4geit/rct-text-selector-component
