#!/usr/bin/env node

const debug = require('debug')('react-packages:utils:rct-unlink-all:bin')
const program = require('commander')
const lib = require('../')
const { version } = require('../package')

async function start() {
  debug('start()')

  program
    .description('Yarn-unlink all the packages.')
    .version(version)
    .parse(process.argv)

  await lib()
}

start()
