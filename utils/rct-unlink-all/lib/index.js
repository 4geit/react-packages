const debug = require('debug')('react-packages:utils:rct-unlink-all:lib')
const { execSync } = require('child_process')
const getPackages = require('./_getPackages')

module.exports = async () => {
  debug('lib')

  getPackages().forEach((p) => {
    debug(`Unlink ${p}`)
    execSync('yarn unlink', { stdio: [0, 1, 2], cwd: p })
  })
}
