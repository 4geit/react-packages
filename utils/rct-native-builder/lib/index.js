const debug = require('debug')('react-packages:utils:rct-project-builder:lib')
const Mustache = require('mustache')
const fs = require('fs')
const path = require('path')
const gitP = require('simple-git/promise')
const _ = require('lodash')
const yaml = require('js-yaml')
const Swagger = require('swagger-client')

const ROOT_FOLDER = path.resolve(process.env.HOME, './Dev')
debug(ROOT_FOLDER)

async function createDirectories({ group, name, directories }) {
  debug('createDirectories()')
  directories.forEach((dir) => {
    const dirPath = `${ROOT_FOLDER}/${group}/${name}/${dir}`
    debug(dirPath)
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath)
      debug(`the new folder ${dir} has been created!`)
    } else {
      debug(`the folder ${dir} already exists!`)
    }
  })
}
async function createFiles({
  group, name, variables, files,
}) {
  debug('createFiles()')
  files.forEach(({ src, dst, rawCopy }) => {
    debug(src)
    debug(dst)
    const srcPath = path.resolve(__dirname, `./template/${src}`)
    const dstPath = `${ROOT_FOLDER}/${group}/${name}/${dst}`
    debug(srcPath)
    debug(dstPath)
    if (!rawCopy) {
      const tpl = fs.readFileSync(srcPath, 'utf8')
      Mustache.parse(tpl, ['<(', ')>'])
      const output = Mustache.render(tpl, variables)
      debug(output)
      fs.writeFileSync(dstPath, output, 'utf8')
    } else {
      fs.copyFileSync(srcPath, dstPath)
    }
    debug(`the file ${dst} has been created!`)
  })
}
async function buildSwaggerClient({ token }) {
  debug('build_swagger_client()')
  const { apis: { default: { ...operations } } } = await new Swagger({
    spec: yaml.safeLoad(fs.readFileSync(path.resolve(__dirname, './swagger/gitlab.yml'), 'utf8')),
    authorizations: { token },
  })
  return operations
}
async function getNamespaceId({ group, getNamespaces }) {
  debug('getNamespaceId()')
  const { body: [{ id: groupNamespaceId }] } = await getNamespaces({ search: group })
  debug(groupNamespaceId)
  return groupNamespaceId
}
async function createRemoteProject({
  name, description, groupNamespaceId, createProject,
}) {
  debug('createRemoteProject()')
  await createProject({
    namespace_id: groupNamespaceId,
    visibility: 'private',
    path: name,
    description: `${description}`,
  })
}
async function switchRunnerOn({ group, name, enableRunner }) {
  debug('switchRunnerOn()')
  await enableRunner({ projectId: `${group}/${name}`, runner_id: process.env.RUNNER_ID })
}
async function prepareRepository({ group, name }) {
  debug('prepareRepository')
  // setup git to working directory
  const git = gitP(`${ROOT_FOLDER}/${group}/${name}`)
  // git init
  await git.init()
  // git add
  await git.add('.')
  // git commit
  await git.commit('feat(init) new repository')
  // add remote repository URL
  await git.addRemote('origin', `git@gitlab.com:${group}/${name}.git`)
  // git push
  await git.push(['-u', 'origin', 'master'])
}
async function usage({ group, name }) {
  /* eslint-disable no-console */
  console.log()
  console.log()
  console.log(`The new repository has been properly generated and is available at https://gitlab.com/${group}/${name}`)
  console.log()
  console.log('You can clone the repository with the following command:')
  console.log()
  console.log(`git clone git@gitlab.com:${group}/${name}.git`)
  console.log()
  console.log('Or just get access to the locally generated folder:')
  console.log()
  console.log(`cd ${ROOT_FOLDER}/${group}/${name}`)
  console.log()
  /* eslint-enable no-console */
}

module.exports = async ({
  group, name, description, authorName, authorEmail, dryRun,
}) => {
  try {
    // debug
    debug(`group: ${group}`)
    debug(`name: ${name}`)
    debug(`description: ${description}`)
    debug(`authorName: ${authorName}`)
    debug(`authorEmail: ${authorEmail}`)
    debug(`dryRun: ${dryRun}`)

    // define variables
    const variables = {
      group,
      name,
      description,
      authorName,
      authorEmail,
      _class: _.upperFirst(_.camelCase(name)),
      _function: _.camelCase(name),
    }
    // define files
    const files = [
      { src: '.babelrc', dst: '.babelrc' },
      { src: '.eslintignore', dst: '.eslintignore' },
      { src: '.eslintrc.yml', dst: '.eslintrc.yml' },
      { src: 'gitignore', dst: '.gitignore' },
      { src: '.gitlab-ci.yml', dst: '.gitlab-ci.yml' },
      { src: '.watchmanconfig', dst: '.watchmanconfig' },
      { src: 'App.js', dst: 'App.js' },
      { src: 'app.json', dst: 'app.json' },
      { src: 'LICENSE', dst: 'LICENSE' },
      { src: 'package.json', dst: 'package.json' },
      { src: 'README.md', dst: 'README.md' },
      { src: '__tests__/App-test.js', dst: '__tests__/App-test.js' },
      { src: 'api/registerForPushNotificationsAsync.js', dst: 'api/registerForPushNotificationsAsync.js' },
      { src: 'assets/fonts/SpaceMono-Regular.ttf', dst: 'assets/fonts/SpaceMono-Regular.ttf', rawCopy: true },
      { src: 'assets/images/icon.png', dst: 'assets/images/icon.png', rawCopy: true },
      { src: 'assets/images/robot-dev.png', dst: 'assets/images/robot-dev.png', rawCopy: true },
      { src: 'assets/images/robot-prod.png', dst: 'assets/images/robot-prod.png', rawCopy: true },
      { src: 'assets/images/splash.png', dst: 'assets/images/splash.png', rawCopy: true },
      { src: 'components/__tests__/StyledText-test.js', dst: 'components/__tests__/StyledText-test.js' },
      { src: 'components/StyledText.js', dst: 'components/StyledText.js' },
      { src: 'constants/Colors.js', dst: 'constants/Colors.js' },
      { src: 'constants/Layout.js', dst: 'constants/Layout.js' },
      { src: 'navigation/MainTabNavigator.js', dst: 'navigation/MainTabNavigator.js' },
      { src: 'navigation/RootNavigation.js', dst: 'navigation/RootNavigation.js' },
      { src: 'screens/HomeScreen.js', dst: 'screens/HomeScreen.js' },
      { src: 'screens/LinksScreen.js', dst: 'screens/LinksScreen.js' },
      { src: 'screens/SettingsScreen.js', dst: 'screens/SettingsScreen.js' },
    ]
    // define directories
    const directories = [
      '',
      '__tests__',
      'api',
      'assets',
      'assets/fonts',
      'assets/images',
      'components',
      'components/__tests__',
      'constants',
      'navigation',
      'screens',
    ]

    // create directories
    await createDirectories({ group, name, directories })
    // create files
    await createFiles({
      group, name, variables, files,
    })
    // build swagger client
    const { getNamespaces, createProject, enableRunner } = await buildSwaggerClient({
      token: process.env.GITLAB_PRIVATE_TOKEN,
    })
    // get namespace id
    const groupNamespaceId = await getNamespaceId({ group, getNamespaces })
    await createRemoteProject({
      name, description, groupNamespaceId, createProject,
    })
    // enable runner
    await switchRunnerOn({ group, name, enableRunner })
    await prepareRepository({ group, name })
    // print usage
    await usage({ group, name })
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
    process.exit(-1)
  }
}
