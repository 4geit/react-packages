/* eslint-disable global-require */
import React from 'react'
import PropTypes from 'prop-types'
import { StatusBar, StyleSheet, View } from 'react-native'
import { AppLoading, Asset, Font } from 'expo'
import { Ionicons } from '@expo/vector-icons'
import { ThemeProvider, COLOR, Toolbar } from 'react-native-material-ui'

import RootNavigation from './navigation/RootNavigation'

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
})

export default class App extends React.Component {
  static propTypes = {
    skipLoadingScreen: PropTypes.bool,
  }
  static defaultProps = {
    skipLoadingScreen: false,
  }

  state = {
    isLoadingComplete: false,
  }

  loadResourcesAsync = async () => Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free
      // to remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ])
  handleLoadingError = (error) => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    // eslint-disable-next-line no-console
    console.warn(error)
  }
  handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  }

  render() {
    // update status bar background color
    StatusBar.setBackgroundColor(COLOR.green700)

    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync}
          onError={this.handleLoadingError}
          onFinish={this.handleFinishLoading}
        />
      )
    }

    return (
      <ThemeProvider uiTheme={uiTheme}>
        <View style={styles.container}>
          {/* Platform.OS === 'ios' && <StatusBar barStyle="default" /> */}
          {/* Platform.OS === 'android' && <View style={styles.statusBarUnderlay} /> */}
          <Toolbar
            leftElement="menu"
            centerElement="My Super App"
            searchable={{
              autoFocus: true,
              placeholder: 'Search',
            }}
          />
          <RootNavigation />
        </View>
      </ThemeProvider>
    )
  }
}
