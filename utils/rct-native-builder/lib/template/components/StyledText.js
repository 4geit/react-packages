import React from 'react'
import PropTypes from 'prop-types'
import { Text } from 'react-native'

const MonoText = props => (
  <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />
)
MonoText.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  style: PropTypes.any,
}
MonoText.defaultProps = {
  style: '',
}

export default MonoText
