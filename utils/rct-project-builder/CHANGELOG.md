# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.103.1"></a>
## [1.103.1](https://gitlab.com/4geit/react-packages/compare/v1.103.0...v1.103.1) (2018-04-10)




**Note:** Version bump only for package @4geit/rct-project-builder

<a name="1.103.0"></a>
# [1.103.0](https://gitlab.com/4geit/react-packages/compare/v1.102.4...v1.103.0) (2018-02-08)


### Features

* **utils:** add new react-native project builder ([7ff7940](https://gitlab.com/4geit/react-packages/commit/7ff7940))




<a name="1.102.4"></a>
## [1.102.4](https://gitlab.com/4geit/react-packages/compare/v1.102.3...v1.102.4) (2018-02-06)


### Bug Fixes

* **project-builder:** fix webpack setup issue ([29a85ce](https://gitlab.com/4geit/react-packages/commit/29a85ce))




<a name="1.102.2"></a>
## [1.102.2](https://gitlab.com/4geit/react-packages/compare/v1.102.1...v1.102.2) (2018-01-30)


### Bug Fixes

* **utils:** fix gitignore issue ([fc3da16](https://gitlab.com/4geit/react-packages/commit/fc3da16))




<a name="1.101.2"></a>
## [1.101.2](https://gitlab.com/4geit/react-packages/compare/v1.101.1...v1.101.2) (2018-01-13)


### Bug Fixes

* **project-builder:** fix minor issue + improved vendor component ([5f28826](https://gitlab.com/4geit/react-packages/commit/5f28826))




<a name="1.101.0"></a>
# [1.101.0](https://gitlab.com/4geit/react-packages/compare/v1.100.1...v1.101.0) (2017-12-19)


### Bug Fixes

* **rct-project-builder:** fix issue related to enableRunner endpoint ([bfe7bdb](https://gitlab.com/4geit/react-packages/commit/bfe7bdb))
* **rct-project-builder:** fix issue related with gitlab createProject endpoint ([d776754](https://gitlab.com/4geit/react-packages/commit/d776754))


### Features

* **utils:** convert scripts to js utils ([cbf6a2b](https://gitlab.com/4geit/react-packages/commit/cbf6a2b))
* **utils:** moved yarn linking related scripts to utils ([111a391](https://gitlab.com/4geit/react-packages/commit/111a391))
